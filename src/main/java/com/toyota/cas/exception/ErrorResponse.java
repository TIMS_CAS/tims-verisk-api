package com.toyota.cas.exception;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@Builder
public class ErrorResponse {

    @JsonIgnore
    private HttpStatus status;

    private Integer statusCode;
    private String message;

    public Integer getStatusCode() {
        return status.value();
    }
}
