package com.toyota.cas.exception;

import com.google.common.collect.Lists;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class EmptyCaseNumberException extends RuntimeException {

    public EmptyCaseNumberException() {
        super("Please provide a valid case Number");
    }
}
