package com.toyota.cas.util.log;

@FunctionalInterface
public interface Filter {
    boolean shouldLog(LogEvent event);
}
