package com.toyota.cas.repository;

        import com.toyota.cas.dao.PersonalInfo;
        import org.junit.Before;
        import org.junit.Test;
        import org.junit.runner.RunWith;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.boot.test.context.SpringBootTest;
        import org.springframework.test.context.ActiveProfiles;
        import org.springframework.test.context.junit4.SpringRunner;

        import static org.junit.Assert.*;
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
public class PersonalInfoRepoTest {
    @Autowired
    private PersonalInfoRepo personalInfoRepo;

    @Test
    public void testFetchData(){
        /*Test data retrieval*/
        PersonalInfo userA = personalInfoRepo.findByVin("1NXBR12E41Z447868");
        assertNotNull(userA);
        assertEquals("SUSAN", userA.getFirst_nm());
    }
}