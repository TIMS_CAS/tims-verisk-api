# TIMS-Verisk-API
This API allows a VERISK employeed to log complaints made about driver scores  
**version = 1.0**

## Controllers
### loadComplaint
**Method: POST**  
This API takes in a `Json` body of the incoming complaint. 
This is a sample body:
```{
         "CASE_NUMBER" : "100",
         "CASE_DATE" : "3/18/2018",
         "VIN": "2BBBCCC1234567890",
         "FIRST_NM": "John",
         "MIDDLE_INITIAL": "m",
         "LAST_NM": "Doe",
         "ORGANIZATION_NM": "Company",
         "ADDRESS_LINE_1": "1234 Fake St",
         "ADDRESS_LINE_2": "",
         "CITY_NM": "Fake City",
         "STATE_CD": "NY",
         "ZIP": "11001",
         "COUNTRY_NM": "USA",
         "PREV_ADDRESS_LINE_1": "123 Main St",
         "PREV_ADDRESS_LINE_2": "",
         "PREV_CITY_NM": "Gardena",
         "PREV_STATE_CD": "CA",
         "PREV_ZIP": "90248",
         "PREV_COUNTRY_NM": "USA",
         "EMAIL_ADDRESS": "thisguys@email.net",
         "CELL_PHONE": "1232732884",
         "HOME_PHONE": "1232732884",
         "WORK_PHONE": "" ,
         "Complaints_Desc" : "I'm actually a great driver"
       }
```

### loadComplaint
**Method: GET**  
This API returns a PDF for a given caseNumber.


## Encryption
We are using `jasypt (Java Simplified Encryption)` to encrypt all our secrets (passwords/usernames).   
Jasypt uses a secret passphrase to encrypt the data, this passphrase needs to be an env variable.
We look for this phrase in `$CAS_SECRET` and needs to be exported locally or set in app service as an environment variable.  
The actual passphrase value should be stored in KeyVault or a similar secure store.  
The values are then entered into our yml files with `ENC()` surrounding it. An example can be seen in `application-dev.yml`.    

It is important to set the provider name for `jasypt` to be set under our `application.yml` file.  
For now, this is set to `SunJCE`. The provider library must be present in all environments that you are running your application.  
 * For more, read  `http://www.jasypt.org/non-default-providers.html`   
 * A program to list all security providers in your env: `https://gist.github.com/nicoulaj/531761` 

You can see the pattern in `application.yml` under `jasypt`.   
To use it, simply insert value in the .`yml` files surrounded by `ENC()`.  
In order to generate the encrypted values, run the jasypt jar as follows after you've imported the jar into your mvn repo.  
~~~~
java -cp ~/.m2/repository/org/jasypt/jasypt/1.9.2/jasypt-1.9.2.jar  org.jasypt.intf.cli.JasyptPBEStringEncryptionCLI input="YOURPASSWORD" password=YOURPASSPHRASE algorithm=PBEWithMD5AndDES providerName=SunJCE
~~~~