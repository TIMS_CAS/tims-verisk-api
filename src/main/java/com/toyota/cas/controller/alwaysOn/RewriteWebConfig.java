package com.toyota.cas.controller.alwaysOn;

import com.toyota.cas.util.log.Level;
import com.toyota.cas.util.log.LogEvent;
import com.toyota.cas.util.log.StructuredLog;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class RewriteWebConfig extends WebMvcConfigurerAdapter {
    private static final StructuredLog logger = StructuredLog.getLog();

    private final RewriteProperties rewriteProperties;

    public RewriteWebConfig(RewriteProperties rewriteProperties) {
        this.rewriteProperties = rewriteProperties;
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        rewriteProperties.getEntries().forEach(
                entry -> {
                    LogEvent.of(RewriteProperties.class)
                            .level(Level.Info)
                            .message(String.format("Mapping source url '%s' to target '%s'", entry.getSourceUrl(), entry.getTarget()))
                            .build()
                            .log(logger);
                    registry.addViewController(entry.getSourceUrl()).setViewName(entry.getTarget());
                }
        );
    }
}
