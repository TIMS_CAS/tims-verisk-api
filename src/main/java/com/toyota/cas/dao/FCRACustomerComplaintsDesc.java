package com.toyota.cas.dao;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Nationalized;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@Table(name = "FCRA_Cust_Complaints_Desc")
@EntityListeners(AuditingEntityListener.class)
public class FCRACustomerComplaintsDesc {

    @Nationalized
    private String comments;
    @Column(name = "case_number")
    private String caseNum;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer commentid;

    @CreatedDate
    @Column(nullable = false, insertable = true, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date create_dtm;
}
