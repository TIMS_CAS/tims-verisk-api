package com.toyota.cas;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.PostConstruct;

@Component
@EnableTransactionManagement
@EnableJpaRepositories
@EnableJpaAuditing
public class ApplicationConfig {

    @Value("${cas.env}")
    private String env;

    @PostConstruct
    public void configExample() {
    }
}
