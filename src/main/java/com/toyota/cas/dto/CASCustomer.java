package com.toyota.cas.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Value;
import org.springframework.cache.annotation.CacheEvict;


import java.util.List;

@Value
@Builder(builderClassName="Builder")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@ApiModel(description = "CAS Customer Information")
@CacheEvict()
public class CASCustomer {
    @ApiModelProperty(notes = "Indicator to see this is a CAS Customer.")
    private boolean isCASCustomer;

    @ApiModelProperty(notes = "List of fields that did not match.")
    private List<String> invalidFields;
}
