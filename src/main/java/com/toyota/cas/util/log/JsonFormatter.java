package com.toyota.cas.util.log;

import com.google.gson.*;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.val;

import java.lang.reflect.Modifier;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class JsonFormatter implements Formatter {

    private static final JsonSerializer<LocalDateTime> localDateTimeJsonSerializer = (dt, type, context) ->
        new JsonPrimitive(dt.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

    private static final JsonSerializer<OffsetDateTime> offsetDateTimeJsonSerializer = (dt, type, context) ->
        new JsonPrimitive(dt.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));

    private static final JsonSerializer<ZonedDateTime> zonedDateTimeJsonSerializer = (dt, type, context) ->
        new JsonPrimitive(dt.format(DateTimeFormatter.ISO_ZONED_DATE_TIME));

    Gson gson;

    public JsonFormatter() {
        this(false);
    }

    public JsonFormatter(boolean prettyPrint) {
        val builder = new GsonBuilder()
        .excludeFieldsWithModifiers(Modifier.STATIC, Modifier.TRANSIENT, Modifier.VOLATILE)
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES)
        .registerTypeAdapter(OffsetDateTime.class, offsetDateTimeJsonSerializer)
        .registerTypeAdapter(LocalDateTime.class, localDateTimeJsonSerializer)
        .registerTypeAdapter(ZonedDateTime.class, zonedDateTimeJsonSerializer);

        if (prettyPrint) {
            builder.setPrettyPrinting();
        }
        gson = builder.create();

    }

    @Override
    public String format(LogEvent event) {
        return gson.toJson(event);
    }

}
