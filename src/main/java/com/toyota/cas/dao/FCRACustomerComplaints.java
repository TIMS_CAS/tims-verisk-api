package com.toyota.cas.dao;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@Table(name = "FCRA_Cust_Complaints")
@EntityListeners(AuditingEntityListener.class)

public class FCRACustomerComplaints {

    @Column(name = "CASE_NUMBER")
    @Id
    private String caseNum;
    private Date case_date;
    private String vin;
    private String first_nm;
    private String middle_initial;
    private String last_nm;
    private String organization_nm;
    private String address_line_1;
    private String address_line_2;
    private String city_nm;
    private String state_cd;
    private String zip;
    private String country_nm;
    private String prev_address_line_1;
    private String prev_address_line_2;
    private String prev_city_nm;
    private String prev_state_cd;
    private String prev_zip;
    private String prev_country_nm;
    private String email_address;
    private String cell_phone;
    private String home_phone;
    private String work_phone;
    private String cas_customer;

    @CreatedDate
    @Column(nullable = false, insertable = true, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date create_dtm;

    @LastModifiedDate
    @Column(nullable = false, insertable = true, updatable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date update_dtm;
}
