package com.toyota.cas.dao;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "PII")
public class PersonalInfo {
    private String hashed_vin;
    private String customer_id;
    @Id
    private String vin;
    private String first_nm;
    private String middle_initial;
    private String last_nm;
    private String organization_nm;
    private String address_line_1;
    private String address_line_2;
    private String city_nm;
    private String state_cd;
    private String zip;
    private String country_nm;
    private String email_address;
    private String cell_phone;
}
