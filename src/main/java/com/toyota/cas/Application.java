package com.toyota.cas;

        import org.springframework.boot.SpringApplication;
        import org.springframework.boot.autoconfigure.SpringBootApplication;

        import javax.annotation.PostConstruct;
        import javax.annotation.PreDestroy;

@SpringBootApplication
//@ComponentScan({"com.microsoft.azure.storage.CloudStorageAccount"})
public class Application {

    @PostConstruct
    public void postConstruct() {
    }

    @PreDestroy
    public void preDestroy() {
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}