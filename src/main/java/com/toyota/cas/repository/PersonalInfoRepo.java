package com.toyota.cas.repository;

import com.toyota.cas.dao.PersonalInfo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonalInfoRepo extends CrudRepository<PersonalInfo, Integer> {
    PersonalInfo findByVin(String name);
}