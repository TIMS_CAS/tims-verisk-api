package com.toyota.cas.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class VinNotAuthorizedException extends RuntimeException {

    public VinNotAuthorizedException(String vin) {
        super("Not Authorized for vin " + vin);
    }
}