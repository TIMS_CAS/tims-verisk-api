package com.toyota.cas.util.log;

@FunctionalInterface
public interface Formatter {
    String format(LogEvent event);
}
