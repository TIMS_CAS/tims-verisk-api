package com.toyota.cas.util.log;

@FunctionalInterface
public interface Writer {
    void log(LogEvent event, String message);
}
