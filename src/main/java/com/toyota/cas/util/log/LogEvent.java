package com.toyota.cas.util.log;

import com.google.common.base.Throwables;
import lombok.Data;
import lombok.NonNull;
import lombok.Singular;
import lombok.Value;

import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;

@Data
@lombok.Builder(builderClassName = "Builder", toBuilder = true)
public final class LogEvent implements Serializable {

    public static Builder of(@NonNull Object source) {
        return LogEvent.builder()
                .source(source.toString())
                .timestamp(OffsetDateTime.now())
                .category(source.getClass().getName())
                .level(Level.Info);
    }

    @NonNull Serializable source;
    @NonNull OffsetDateTime timestamp;
    @NonNull String category;
    @NonNull Level level;
    String id;
    String message;
    ProcessingStep processingStep;
    String adlsFileLocation;
    Integer adlsLineNumber;
    Integer adlsAvroOffset;
    Integer kafkaPartition;
    Long kafkaOffset;
    // Generic representation for amount of time to complete specific steps during message processing, as well as
    // time to write Tuple to ADLS (in milliseconds)
    Long processingTime;

    Integer docdbInsertCount;
    Double docdbRequestCharge;

    /**
     * Header key-value pairs that help distinguish this log event from other log events
     */
    @Singular Map<String, Serializable> headers;
    Body body;
    Error error;

    /**
     * List of tags or labels used to group and/or search for specific log events
     */
    @Singular List<String> tags;

    /**
     * Any additional meta data that should be included in the log event that's useful for debugging or troubleshooting issues
     */
    @Singular("trace") Map<String, Serializable> trace;

    /**
     * Sample of the data associated with this log event. Used for inspecting values during debugging
     */
    @Value(staticConstructor = "of")
    public static class Body implements Serializable {
        public static Body of(Serializable contents) {
            return of(contents.getClass().getName(), contents);
        }
        String type;
        Serializable contents;
    }

    /**
     * An object containing information about the error that triggered this log event
     */
    @Value(staticConstructor = "of")
    public static class Error implements Serializable {

        public static Error of(Throwable thrown) {
            List<String> contexts = null;
            Long bitPosition = null;

            return of(thrown.getClass().getName(), thrown.getMessage(), new Thrown(thrown), null, null, contexts, bitPosition);
        }

        String type;
        String message;
        Thrown thrown;
        String code;
        String debug;
        List<String> decoderContexts;
        Long bitPosition;
    }

    @Value
    private static class Thrown implements Serializable {
        String message;
        String type;
        StackTraceElement[] stacktrace;

        public Thrown(Throwable th) {
            Throwable cause = Throwables.getRootCause(th);
            if (cause == null)
                cause = th;
            this.message =  cause.getMessage();
            this.type = cause.getClass().getName();
            this.stacktrace = cause.getStackTrace();
        }

    }

    public String format(Formatter formatter) {
        return formatter.format(this);
    }

    public boolean isError() {
        return error != null;
    }

    public boolean hasThrown() {
        return isError() && error.getThrown() != null;
    }

    public LogEvent log(StructuredLog logger) {
        logger.log(this);
        return this;
    }
}
