package com.toyota.cas.controller.swagger;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.endpoint.mvc.EndpointHandlerMapping;
import org.springframework.boot.actuate.endpoint.mvc.MvcEndpoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.ApiSelectorBuilder;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;

import java.util.Set;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Value("${swagger.host}")
    private String swaggerHost;
    @Value("${swagger.baseUrl}")
    private String swaggerBaseUrl;

    @Bean
    public Docket swaggerSpringMvcPlugin(final EndpointHandlerMapping actuatorEndpointHandlerMapping) {
        ApiSelectorBuilder builder = new Docket(DocumentationType.SWAGGER_2)
            .apiInfo(apiInfo())
            .host(swaggerHost + swaggerBaseUrl)
            .select()
            .apis(RequestHandlerSelectors.any())
            .paths(paths());


        // Ignore the spring-boot-actuator endpoints:
        Set<MvcEndpoint> endpoints = actuatorEndpointHandlerMapping.getEndpoints();
        endpoints.forEach(endpoint -> {
            String path = endpoint.getPath();
            builder.paths(Predicates.not(PathSelectors.regex(path + ".*")));
        });

        return builder
            .build()
            .securitySchemes(Lists.newArrayList(
                new ApiKey("Ocp-Apim-Subscription-Key", "Ocp-Apim-Subscription-Key", "header"),
                new ApiKey("subscription-key", "subscription-key", "query")));
    }

    private Predicate<String> paths() {
        return Predicates.not(PathSelectors.regex("/error.*"));
    }


    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("CAS Verisk API")
            .description("This is the Verisk API to load and view complaints from CAS")
            .version("1.0")
            .build();
    }
}