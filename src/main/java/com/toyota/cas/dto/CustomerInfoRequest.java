package com.toyota.cas.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.toyota.cas.exception.FutureDateException;
import com.toyota.cas.exception.InvalidDateException;
import lombok.Builder;
import lombok.Value;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

@Value
@Builder(builderClassName = "Builder")
public class CustomerInfoRequest {

    @JsonProperty("CASE_DATE")
    private String caseDate;
    @JsonProperty("VIN")
    private String vin;
    @JsonProperty("FIRST_NM")
    private String firstName;
    @JsonProperty("MIDDLE_INITIAL")
    private String middleInitial;
    @JsonProperty("LAST_NM")
    private String lastName;
    @JsonProperty("ZIP")
    private String zipCode;
    @JsonProperty("CASE_NUMBER")
    private String caseNumber;
    @JsonProperty("ORGANIZATION_NM")
    private String organizationName;
    @JsonProperty("ADDRESS_LINE_1")
    private String addressLine1;
    @JsonProperty("ADDRESS_LINE_2")
    private String addressLine2;
    @JsonProperty("CITY_NM")
    private String cityName;
    @JsonProperty("STATE_CD")
    private String state;
    @JsonProperty("COUNTRY_NM")
    private String countryName;
    @JsonProperty("PREV_ADDRESS_LINE_1")
    private String prevAddressLine1;
    @JsonProperty("PREV_ADDRESS_LINE_2")
    private String prevAddressLine2;
    @JsonProperty("PREV_CITY_NM")
    private String prevCityName;
    @JsonProperty("PREV_STATE_CD")
    private String prevStateCD;
    @JsonProperty("PREV_ZIP")
    private String prevZip;
    @JsonProperty("PREV_COUNTRY_NM")
    private String prevCountryName;
    @JsonProperty("EMAIL_ADDRESS")
    private String emailAddress;
    @JsonProperty("CELL_PHONE")
    private String cellPhone;
    @JsonProperty("HOME_PHONE")
    private String homePhone;
    @JsonProperty("WORK_PHONE")
    private String workPhone;
    @JsonProperty("Complaints_Desc")
    private String comment;
    private String isCasCustomer;

    public Date getParsedCaseDate() {
        Date date;
        SimpleDateFormat formatter = new SimpleDateFormat("MM/d/yyyy");
        try {
            date = formatter.parse(caseDate);

            if (date.toInstant().isAfter(Instant.now())) {

                throw new FutureDateException(caseDate);
            }
        } catch (ParseException e) {
            throw new InvalidDateException(caseDate);
        }
        return date;
    }
}