package com.toyota.cas.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidDateException extends RuntimeException {

    public InvalidDateException(String dt) {
        super(String.format("Bad Date passed in. [%s] Please follow format: MM/d/yyyy", dt));
    }
}