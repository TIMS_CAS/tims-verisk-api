package com.toyota.cas.exception;

import com.microsoft.applicationinsights.telemetry.Telemetry;
import com.toyota.cas.util.log.Level;
import com.toyota.cas.util.log.LogEvent;
import com.toyota.cas.util.log.ProcessingStep;
import com.toyota.cas.util.log.StructuredLog;
import org.apache.logging.log4j.core.config.Order;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;

@Order(Ordered.HIGHEST_PRECEDENCE)
@RestControllerAdvice
public class TIMSExceptionHandler {
    private static final StructuredLog logger = StructuredLog.getLog();

    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse missingParamException(MissingServletRequestParameterException ex, HttpServletResponse response) {

        // log, build and return error
        return handleError(ex.getMessage(), ex, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ServletRequestBindingException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse missingParamException(ServletRequestBindingException ex, HttpServletResponse response) {

        // log, build and return error
        return handleError(ex.getMessage(), ex, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse missingCaseNum(HttpMessageNotReadableException ex, HttpServletResponse response) {

        // log, build and return error
        return handleError(("Bad JSON passed. Please check json and date [dd/mm/yyyy] formats."), ex, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EmptyCaseNumberException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse missingCaseNum(EmptyCaseNumberException ex, HttpServletResponse response) {

        // log, build and return error
        return handleError(ex.getMessage(), ex, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(PDFNotAvailableException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse missingPDF(PDFNotAvailableException ex, HttpServletResponse response) {

        // log, build and return error
        return handleError(ex.getMessage(), ex, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(VinNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse entityNotFound(VinNotFoundException ex, HttpServletResponse response) {

        // log, build and return error
        return handleError(ex.getMessage(), ex, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InvalidCaseNumberException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse entityNotFound(InvalidCaseNumberException ex, HttpServletResponse response) {

        // log, build and return error
        return handleError(ex.getMessage(), ex, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InvalidFieldsException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse badFields(InvalidFieldsException ex, HttpServletResponse response) {

        // log, build and return error
        return handleError(ex.getMessage(), ex, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(VinNotAuthorizedException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ErrorResponse entityNotFound(VinNotAuthorizedException ex, HttpServletResponse response) {

        // log, build and return error
        return handleError(ex.getMessage(), ex, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(InvalidDateException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse badDate(InvalidDateException ex, HttpServletResponse response) {

        // log, build and return error
        return handleError(ex.getMessage(), ex, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(FutureDateException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse badDate(FutureDateException ex, HttpServletResponse response) {

        // log, build and return error
        return handleError(ex.getMessage(), ex, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(TBDCErrors.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse tbdcErrorResponse(TBDCErrors errorCodes, HttpServletResponse response) {

        // build and return error
        return handleError(errorCodes.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse genericErrorResponse(Exception ex, HttpServletResponse response) {

        // build and return error
        return handleError("Something went wrong. Please contact CASupport <CAS@toyotaconnected.com>", ex, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /* Log and create error response */
    private ErrorResponse handleError(String message, HttpStatus statusCode) {
        return handleError(message, null, statusCode);
    }

    private ErrorResponse handleError(String message, Exception ex, HttpStatus statusCode) {
        LogEvent.Builder eventBuilder = LogEvent.of(Telemetry.class)
                .level(Level.Error)
                .processingStep(ProcessingStep.GENERAL_EXCEPTION)
                .message("Exception Thrown.");

        // exception may or may not be present
        if (ex != null) {
            eventBuilder
                    .error(LogEvent.Error.of(ex))
                    .build().log(logger);
        } else {
            eventBuilder.build().log(logger);
        }

        return ErrorResponse.builder().message(message).status(statusCode).build();
    }
}
