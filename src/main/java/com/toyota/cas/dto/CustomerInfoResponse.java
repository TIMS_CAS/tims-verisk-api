package com.toyota.cas.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.Value;
import org.springframework.cache.annotation.CacheEvict;

@JsonInclude(JsonInclude.Include.NON_EMPTY)

@Builder(builderClassName = "Builder")

@ApiModel(description = "Customer Information")
@CacheEvict(allEntries = true)
public class CustomerInfoResponse {
    @ApiModelProperty(notes = "Case Number", required = true)
    public String caseNumber;

    @ApiModelProperty(notes = "The CAS Customer details.", required = true)

    public CASCustomer casCustomer;
}