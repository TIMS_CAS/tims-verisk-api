package com.toyota.cas.service;

import com.google.common.collect.Lists;
import com.toyota.cas.config.FCRAEmailConfig;
import com.toyota.cas.dao.FCRACustomerComplaints;
import com.toyota.cas.dao.FCRACustomerComplaintsDesc;
import com.toyota.cas.dao.PersonalInfo;
import com.toyota.cas.dto.CASCustomer;
import com.toyota.cas.dto.CustomerInfoRequest;
import com.toyota.cas.dto.CustomerInfoResponse;
import com.toyota.cas.exception.VinNotFoundException;
import com.toyota.cas.repository.FCRACustomerComplaintsDescRepo;
import com.toyota.cas.repository.FCRACustomerComplaintsRepo;
import com.toyota.cas.repository.PersonalInfoRepo;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

import static java.util.Optional.ofNullable;

@Service
public class CustomerComplaintService {

    @Autowired
    PersonalInfoRepo personalInfoRepo;

    @Autowired
    FCRACustomerComplaintsRepo complaintsRepo;

    @Autowired
    FCRACustomerComplaintsDescRepo complaintsDescRepo;

    @Autowired
    SendGridEmailService sendGridEmailService;

    @Autowired
    FCRAEmailConfig emailConfig;


    String casCustomer;

    public CustomerInfoResponse addCustomerComplaintDetails(CustomerInfoRequest request) {

        CustomerInfoResponse response;
        List<String> invalidFields=null;
        String vin = request.getVin();

        //Check if this is a valid VIN in PersonalInfoRepo table
        PersonalInfo customerPII = personalInfoRepo.findByVin(vin);
        if (customerPII != null) {
            casCustomer = "N";
            invalidFields = ValidateUser.validatePersonalInfo(request, customerPII);

            if (invalidFields.isEmpty()) {
                //This is a CAS Customer
                casCustomer = "Y";
                //sendEmailNotification(request, invalidFields);


            }


            FCRACustomerComplaints complaints = complaintsRepo.findByCaseNum(request.getCaseNumber());
            System.out.print("Country Name::: "+ request.getPrevAddressLine1() + '\n');
            System.out.print("Email Address::" + ofNullable(request.getEmailAddress()).orElse(""));
            saveComplaints(complaints, request, customerPII,casCustomer,invalidFields);

            List<FCRACustomerComplaintsDesc> complaintsDesc = complaintsDescRepo.findByCaseNum(request.getCaseNumber());
            saveComplaintDesc(complaintsDesc, request);
        } else {
            //This is not a CAS Customer

            FCRACustomerComplaints complaints = complaintsRepo.findByCaseNum(request.getCaseNumber());

            System.out.print("Country Name::: "+ request.getCountryName() + '\n');
            saveComplaints(complaints, request, customerPII, casCustomer,invalidFields);

            List<FCRACustomerComplaintsDesc> complaintsDesc = complaintsDescRepo.findByCaseNum(request.getCaseNumber());
            saveComplaintDesc(complaintsDesc, request);

            throw new VinNotFoundException(vin);
        }


        CASCustomer customer = CASCustomer.builder()
                .invalidFields(invalidFields)
                .isCASCustomer(BooleanUtils.toBoolean(casCustomer, "Y", "N"))
                .build();


        response = CustomerInfoResponse.builder()
                .casCustomer(customer)
                .caseNumber(request.getCaseNumber())
                .build();

        return response;
    }




    public void saveComplaints(FCRACustomerComplaints complaints, CustomerInfoRequest request, PersonalInfo personalInfo, String casCustomer,List<String>invalidFields) {

        complaints = ofNullable(
                complaints).orElse(new FCRACustomerComplaints());

        complaints.setCaseNum(request.getCaseNumber());
        complaints.setCas_customer(casCustomer);
        complaints.setCase_date(request.getParsedCaseDate());
        complaints.setVin(request.getVin().toUpperCase());
        complaints.setFirst_nm(request.getFirstName().toUpperCase());
        complaints.setMiddle_initial(ofNullable(request.getMiddleInitial()).orElse("").toUpperCase());
        complaints.setLast_nm(ofNullable(request.getLastName()).orElse("").toUpperCase());
        complaints.setOrganization_nm(ofNullable(request.getOrganizationName()).orElse("").toUpperCase());
        complaints.setAddress_line_1(Address(request,personalInfo));
        complaints.setAddress_line_2(ofNullable(request.getAddressLine2()).orElse(""));
        complaints.setCity_nm(ofNullable(request.getCityName()).orElse("").toUpperCase());
        complaints.setState_cd(ofNullable(request.getState()).orElse("").toUpperCase());
        complaints.setZip(ZipCode(personalInfo,request,invalidFields));
        complaints.setCountry_nm(ofNullable(request.getCountryName()).orElse("").toUpperCase());
        complaints.setPrev_address_line_1(ofNullable(request.getPrevAddressLine1()).orElse(""));
        complaints.setPrev_address_line_2(ofNullable(request.getPrevAddressLine2()).orElse(""));
        complaints.setPrev_city_nm(ofNullable(request.getPrevCityName()).orElse(""));
        complaints.setPrev_state_cd(ofNullable(request.getPrevStateCD()).orElse(""));
        complaints.setPrev_zip(ofNullable(request.getPrevZip()).orElse(""));
        complaints.setPrev_country_nm(ofNullable(request.getPrevCountryName()).orElse("").toUpperCase());
        complaints.setEmail_address(ofNullable(request.getEmailAddress()).orElse("").toUpperCase());
        complaints.setCell_phone(ofNullable(request.getCellPhone()).orElse(""));
        complaints.setHome_phone(ofNullable(request.getHomePhone()).orElse(""));
        complaints.setWork_phone(ofNullable(request.getWorkPhone()).orElse(""));
                        complaintsRepo.save(complaints);
    }

    public String ZipCode(PersonalInfo personalInfo, CustomerInfoRequest request,List<String> invalidFields ){

        if(request.getZipCode().isEmpty() || request.getZipCode()==null){
            return  Optional.ofNullable(request.getZipCode()).orElse("");
        }
        if(invalidFields.contains("ZipCode")){
            return request.getZipCode();
        }else{
            return personalInfo.getZip();

        }

    }

    public  String Address(CustomerInfoRequest request, PersonalInfo personalInfo){
        if(request.getAddressLine1().isEmpty() || request.getAddressLine1()==null) {
            return request.getAddressLine1();
        }
        if(!Optional.ofNullable(request.getAddressLine1()).orElse("").trim().toLowerCase()
                .replaceAll("[^0-9]", "").equalsIgnoreCase(Optional.ofNullable(personalInfo.getAddress_line_1())
                        .orElse("").trim().toLowerCase().replaceAll("[^0-9]", ""))){
            return  request.getAddressLine1();
        }else {
            return personalInfo.getAddress_line_1();
        }
      }

    private void saveComplaintDesc(List<FCRACustomerComplaintsDesc> complaintsDescs, CustomerInfoRequest request) {

        FCRACustomerComplaintsDesc thisComplaint = new FCRACustomerComplaintsDesc();

        if (!complaintsDescs.isEmpty()) {

            for (FCRACustomerComplaintsDesc complaintsDesc : complaintsDescs) {

                if (StringUtils.equalsIgnoreCase(complaintsDesc.getComments(), request.getComment())) {
                    //Found a matching comment
                    return;
                }
            }
        }

        thisComplaint.setCaseNum(request.getCaseNumber());
        thisComplaint.setComments(request.getComment());

        complaintsDescRepo.save(thisComplaint);
    }

    private void sendEmailNotification(CustomerInfoRequest request, List<String> invalidFields) {

        String bodyTag = "<p>&nbsp;</p>" +
                "<p>&nbsp;</p>" +
                "<p><span style=\"color: #000000; background-color: #ffffff;\">This message&nbsp;was&nbsp;generated by&nbsp;Versisk API. | For&nbsp;questions please contact&nbsp;"
                + emailConfig.sendFrom
                + "</span></span></p>";

        String body = String.format("<h1><strong>Case Number:</strong>&nbsp;%s</h1>" +
                        "<p><strong>FirstName: </strong>%s</p>" +
                        "<p><strong>LastName:</strong> %s</p>" +
                        "<p><strong>Address:</strong> %s</p>" +
                        "<p><strong>Zip:</strong> %s</p>" +
                        "<p>&nbsp;</p>" +
                        "<h4 style=\"color: #5e9ca0;\"><span style=\"background-color: #ffcc00; color: #000000;\"><strong>InvalidFields:</strong>&nbsp;&nbsp;</span></h4>" +
                        "<h4 style=\"color: #5e9ca0;\"><span style=\"color: #000000; background-color: #ffffff;\">[First Name, Last Name, ZipCode] </span></h4>",
                request.getCaseNumber(),
                request.getFirstName().toUpperCase(),
                request.getLastName().toUpperCase(),
                request.getAddressLine1().toUpperCase(),
                request.getZipCode(),
                invalidFields.isEmpty() ? "none" : Lists.newArrayList(invalidFields)
        ) + bodyTag;

        String subject = String.format("Verisk | CaseNumber: [%s]", request.getCaseNumber());

        sendGridEmailService.sendHTML(emailConfig.sendFrom, emailConfig.sendTo, subject, body);
    }
}

