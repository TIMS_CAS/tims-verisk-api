package com.toyota.cas.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class FutureDateException extends RuntimeException {

    public FutureDateException(String dt) {
        super(String.format("Invalid Date! Future date passed in: [%s]", dt));
    }
}