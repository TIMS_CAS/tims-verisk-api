package com.toyota.cas.service;

import java.util.List;

public interface EmailService {

    int sendText(String from, List<String> to, String subject, String body);

    int sendHTML(String from, List<String> to, String subject, String body);
}