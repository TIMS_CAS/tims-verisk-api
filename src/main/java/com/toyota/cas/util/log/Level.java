package com.toyota.cas.util.log;

public enum Level {

    Trace, Debug, Info, Warn, Error, Fatal, None;

    public boolean isNone() { return  this == None; }

    public boolean isTrace() { return compareTo(Trace) >= 0; }

    public boolean isDebug() { return compareTo(Debug) >= 0; }

    public boolean isInfo() {
        return compareTo(Info) >= 0;
    }

    public boolean isWarn() {
        return compareTo(Warn) >= 0;
    }

    public boolean isError() {
        return compareTo(Error) >= 0;
    }

    public boolean isFatal() {
        return compareTo(Fatal) >= 0;
    }
}
