package com.toyota.cas.exception;

import com.google.common.collect.Lists;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class InvalidFieldsException extends RuntimeException {

    public InvalidFieldsException(List<String> invalidFields) {
        super("You have provided invalid fields for " + Lists.newArrayList(invalidFields));
    }
}
