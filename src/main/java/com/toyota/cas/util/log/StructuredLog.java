package com.toyota.cas.util.log;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Slf4j
@Builder(builderClassName = "Builder")
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public final class StructuredLog {

    public static StructuredLog getLog() {
        return defaultLog;
    }

    public static StructuredLog.Builder of() {
        return builder()
        .filter(defaultFilter)
        .formatter(defaultFormatter)
        .writer(defaultWriter);
    }

    private static final Filter defaultFilter = (event) -> true;
    private static final Formatter defaultFormatter = new JsonFormatter();
    private static final Writer defaultWriter = (event, message) -> {
        Logger logger = LoggerFactory.getLogger(event.getCategory());

        switch (event.getLevel()) {
            case Trace:
                logger.trace(message);
                break;
            case Debug:
                logger.debug(message);
                break;
            case Info:
                logger.info(message);
                break;
            case Warn:
                logger.warn(message);
                break;
            case Error:
                logger.error(message);
                break;
            case Fatal:
                logger.error(message);
                break;
        }
    };
    private static final StructuredLog defaultLog = of().build();

    @NonNull Formatter formatter;
    @NonNull Filter filter;
    @NonNull
    Writer writer;

    //removing the body from log files, as the payload
    //is fairly large
    public void log(LogEvent event) {
        LogEvent eventWithoutBody = event.toBuilder().build();

        eventWithoutBody.setBody(null);

        if (!eventWithoutBody.getLevel().isNone() && filter.shouldLog(eventWithoutBody)) {
            try {
                String msg = eventWithoutBody.format(formatter);
                writer.log(eventWithoutBody, msg);
            } catch (Exception e) {
                log.error("Failed to write LogEvent: %s:%s", eventWithoutBody.getSource(), eventWithoutBody.getId(), e);
            }
        }
    }
}
