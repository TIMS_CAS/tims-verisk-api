package com.toyota.cas.exception;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class TBDCErrors extends RuntimeException {
    private String errCode;
    private String errMsg;

    public TBDCErrors(String errCode, String errMsg) {
        this.errCode = errCode;
        this.errMsg = errMsg;
    }
}