package com.toyota.cas.repository;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import com.toyota.cas.config.BlobConfig;
import com.toyota.cas.exception.PDFNotAvailableException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.InputStream;

@Repository
public class CASBlobPDFReport {

    @Autowired
    private BlobConfig blobConfig;

    public byte[] getCasePDF(String caseNumber) throws Exception {

        CloudStorageAccount cloudStorageAccount = CloudStorageAccount.parse(blobConfig.getConnectionString());

        CloudBlobClient serviceClient = cloudStorageAccount.createCloudBlobClient();
        CloudBlobContainer container = serviceClient.getContainerReference(blobConfig.containerName);

        CloudBlockBlob blob = container.getBlockBlobReference(caseNumber + ".pdf");

        byte[] binaryData;

        if (blob.exists()) {
            InputStream openStream = blob.openInputStream();
            binaryData = new byte[(int) blob.getProperties().getLength()];
            openStream.read(binaryData);
            openStream.close();
        } else {
            throw new PDFNotAvailableException(caseNumber);
        }

        return binaryData;
    }
}
