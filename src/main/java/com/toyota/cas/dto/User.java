package com.toyota.cas.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.Wither;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Value
@Builder
@Wither
@AllArgsConstructor
public class User {

    @ApiModelProperty(notes = "ID of user", required =true, hidden=true)
    @Id
    @NotNull(
            message = "User ID must not be null",
            groups = Existing.class)
    @Null(
            message = "User ID must be null for new user",
            groups = New.class)
    private String userId;

    @ApiModelProperty(notes = "First Name of user" , readOnly =true, required=true)
    @NotNull(
            message = "First name is required",
            groups = {Existing.class, New.class})
    private String fname;

    @ApiModelProperty(notes = "Last Name of user" , required=true)
    @NotNull(
            message = "Last name is required",
            groups = {Existing.class, New.class})
    private String lname;

    @ApiModelProperty(notes = "Age of user")
    private Integer age;

    public interface Existing {
    }

    public interface New {
    }
}