package com.toyota.cas.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class InvalidCaseNumberException extends RuntimeException {

    public InvalidCaseNumberException(String caseNumber) {
        super(String.format("Case Number [%s] was not found.", caseNumber));
    }
}
