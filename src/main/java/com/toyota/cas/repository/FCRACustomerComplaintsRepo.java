package com.toyota.cas.repository;

import com.toyota.cas.dao.FCRACustomerComplaints;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FCRACustomerComplaintsRepo extends CrudRepository<FCRACustomerComplaints, Integer> {
    FCRACustomerComplaints findByCaseNum(String caseNumber);
}