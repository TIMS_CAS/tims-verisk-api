package com.toyota.cas.service;

import com.toyota.cas.dao.PersonalInfo;
import com.toyota.cas.dto.CustomerInfoRequest;
import org.apache.commons.lang3.StringUtils;
import java.util.Optional;

import java.util.ArrayList;
import java.util.List;

public class ValidateUser {

    public static List<String> validatePersonalInfo(CustomerInfoRequest input, PersonalInfo pii) {

        List<String> invalidFields = new ArrayList<>();

        if (!StringUtils.equalsIgnoreCase(input.getFirstName(), pii.getFirst_nm())) {
            invalidFields.add("First Name");
        }

        if (!StringUtils.equalsIgnoreCase(input.getLastName(), pii.getLast_nm())) {
            invalidFields.add("Last Name");
        }

        if (Optional.ofNullable(input.getZipCode()).orElse("").length() <= 4 ||!(Optional.ofNullable(input.getZipCode()).orElse("").toString().substring(0, 5).trim()
                .startsWith(Optional.ofNullable(pii.getZip()).orElse("").toString().substring(0, 5).trim()))) {
            invalidFields.add("ZipCode");
        }

        if (!Optional.ofNullable(input.getAddressLine1()).orElse("").trim().toLowerCase()
                .replaceAll("[^0-9]", "").equalsIgnoreCase(Optional.ofNullable(pii.getAddress_line_1())
                        .orElse("").trim().toLowerCase().replaceAll("[^0-9]", ""))) {
            invalidFields.add("Address Line1");
        }

        return invalidFields;
    }
}
