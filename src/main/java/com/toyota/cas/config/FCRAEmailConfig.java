package com.toyota.cas.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Map;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "FCRAEmail")
public class FCRAEmailConfig {

    public String sendFrom;
    public List<String> sendTo;

}