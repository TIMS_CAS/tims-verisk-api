package com.toyota.cas.controller;

public interface BaseController {
    String SUBSCRIPTION_KEY = "ocp-apim-subscription-key";
    String SUBSCRIPTION_EMAIL = "request-email";

}
