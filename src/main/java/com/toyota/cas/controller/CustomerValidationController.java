package com.toyota.cas.controller;

import com.toyota.cas.dto.CustomerInfoRequest;
import com.toyota.cas.dto.CustomerInfoResponse;
import com.toyota.cas.exception.EmptyCaseNumberException;
import com.toyota.cas.service.CustomerComplaintService;
import com.toyota.cas.service.FCRAReportPDF;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Api(value = "FCRA Customer", description = "Rest API to register complaints")
public class CustomerValidationController implements BaseController {

    @Autowired
    CustomerComplaintService processor;

    @Autowired
    FCRAReportPDF fcraReportPDF;

    @RequestMapping(value = {"/v1/loadComplaint"}, method = {RequestMethod.POST})
    public CustomerInfoResponse loadComplaint(@RequestBody @Valid CustomerInfoRequest customerInfoRequest) {

        //Try to parse date
        customerInfoRequest.getParsedCaseDate();

        if (customerInfoRequest.getCaseNumber() == null || customerInfoRequest.getCaseNumber().isEmpty()) {
            throw new EmptyCaseNumberException();
        }

        return processor.addCustomerComplaintDetails(customerInfoRequest);
    }

    @RequestMapping(value = {"/v1/getReport"}, method = {RequestMethod.GET}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> generateReport(@RequestParam(value = "caseNumber") String caseNumber) throws Exception {

        HttpHeaders headers = new HttpHeaders();

        byte[] pdfByte = fcraReportPDF.retreiveReport(caseNumber);

        headers.setContentDispositionFormData("attachment", caseNumber + ".pdf");
        headers.setContentLength(pdfByte.length);
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        headers.setContentType(MediaType.APPLICATION_PDF);

        ResponseEntity<byte[]> FCRAReport = new ResponseEntity<byte[]>(pdfByte, headers, HttpStatus.OK);

        return FCRAReport;
    }
}
