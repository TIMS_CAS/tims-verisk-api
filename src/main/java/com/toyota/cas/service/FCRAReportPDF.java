package com.toyota.cas.service;

import com.toyota.cas.exception.InvalidCaseNumberException;
import com.toyota.cas.repository.CASBlobPDFReport;
import com.toyota.cas.repository.FCRACustomerComplaintsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FCRAReportPDF {

    @Autowired
    CASBlobPDFReport casBlobPDFReport;

    @Autowired
    FCRACustomerComplaintsRepo fcraCustomerComplaintsRepo;

    public byte[] retreiveReport(String caseNumber) throws Exception {

        //validate CaseNumbr
        if (fcraCustomerComplaintsRepo.findByCaseNum(caseNumber) == null) {
            throw new InvalidCaseNumberException(caseNumber);
        }
        return casBlobPDFReport.getCasePDF(caseNumber);
    }
}
