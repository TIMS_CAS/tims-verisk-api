package com.toyota.cas.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "azure")
public class BlobConfig {

    @Value("${azure.storage.connection-string}")
    public String ConnectionString;
    public String containerName;
}