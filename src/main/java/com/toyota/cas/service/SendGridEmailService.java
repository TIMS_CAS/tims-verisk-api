package com.toyota.cas.service;

import com.sendgrid.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class SendGridEmailService implements EmailService {

    @Autowired
    private SendGrid sendGridClient;

    @Override
    public int sendText(String from, List<String> to, String subject, String body) {
        return sendEmail(from, to, subject, new Content("text/plain", body));

    }

    @Override
    public int sendHTML(String from, List<String> to, String subject, String body) {
        return sendEmail(from, to, subject, new Content("text/html", body));
    }

    private int sendEmail(String from, List<String> to, String subject, Content content) {

        Personalization personalization = new Personalization();
        for (String email : to) {
            personalization.addTo(new Email(email));
        }

        Mail mail = new Mail();
        mail.setFrom(new Email(from));
        mail.addPersonalization(personalization);
        mail.setSubject(subject);
        mail.addContent(content);

        Request request = new Request();
        Response response = new Response();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            response = sendGridClient.api(request);

            System.out.println(response.getStatusCode());
            System.out.println(response.getBody());
            System.out.println(response.getHeaders());
        } catch (IOException ex) {
            //Todo - Add logging for exception
        }

        return response.getStatusCode();
    }
}