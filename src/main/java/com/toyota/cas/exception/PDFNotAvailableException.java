package com.toyota.cas.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class PDFNotAvailableException extends RuntimeException {

    public PDFNotAvailableException(String caseNumber) {
        super(String.format("The PDF for case number [%s] was not found.", caseNumber));
    }
}
