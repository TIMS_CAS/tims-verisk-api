package com.toyota.cas.repository;

import com.toyota.cas.dao.FCRACustomerComplaintsDesc;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FCRACustomerComplaintsDescRepo extends CrudRepository<FCRACustomerComplaintsDesc, Integer> {

    List<FCRACustomerComplaintsDesc> findByCaseNum(String caseNumber);
}